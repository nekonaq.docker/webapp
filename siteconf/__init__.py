import os

PROJECT = 'webapp'
BASE_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

from . import celery                       # noqa: F401,E402

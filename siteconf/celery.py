import os
from celery import Celery
from . import BASE_DIR
from djangoexpack.utils.settings import detect_settings_module

if 'DJANGO_SETTINGS_MODULE' not in os.environ:
    os.environ['DJANGO_SETTINGS_MODULE'] = detect_settings_module(BASE_DIR)

print("# CELERY.DJANGO_SETTINGS_MODULE={}".format(os.environ.get('DJANGO_SETTINGS_MODULE')))

# from django.apps import apps
# print("##", apps.loading, apps.ready)

# import django                              # noqa: E402
# django.setup()

from django.conf import settings           # noqa: E402

app = Celery(settings.CELERY_NAME)
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
    return 'OK'

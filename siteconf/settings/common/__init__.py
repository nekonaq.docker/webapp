import os
from socket import gethostname
from djangoexpack.utils.settings import load_settings_from_config
from .. import BASE_DIR, PROJECT

PROJECT = os.environ.get('DJANGO_PROJECT_NAME', PROJECT)

settings_name = os.environ['DJANGO_SETTINGS_MODULE']
ENVIRONMENT = settings_name.rsplit('.', 1)[1] or 'local'

from .base import *                        # noqa: F401,F403,E402

print('#', ' '.join(
    ('='.join([key, globals().get(key, '')])
     for key in ('PROJECT', 'BASE_DIR', 'ENVIRONMENT'))
))

print("# cwd: {}".format(os.getcwd()))

locals().update(load_settings_from_config(
    [os.path.join(BASE_DIR, 'config', 'hiera.yaml'), os.path.join(BASE_DIR, 'config', 'hiera.yml')],
    PROJECT=PROJECT,
    BASE_DIR=BASE_DIR,
    ENVIRONMENT=ENVIRONMENT,
    environment=ENVIRONMENT,               # for hiera compatibility
    hostname=gethostname(),
))

import os
from .. import BASE_DIR, PROJECT           # noqa: F401

if os.environ.get('DJANGO_SETTINGS_MODULE') == __name__:
    os.environ['DJANGO_SETTINGS_MODULE'] = '{}.local'.format(__name__)
    from .common import *                  # noqa: F401,F403

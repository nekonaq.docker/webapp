from django.urls import path, include
from django.contrib import admin
from django.conf import settings

urlpatterns = [
    path('', admin.site.urls),
]

try:
    settings.INSTALLED_APPS.index('django.contrib.admindocs')
    urlpatterns.append(
        path('doc/', include('django.contrib.admindocs.urls')),
    )
except ValueError:
    pass

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

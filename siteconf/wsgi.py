import os
from django.core.wsgi import get_wsgi_application
from djangoexpack.utils.settings import detect_settings_module


base_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
os.chdir(base_dir)

# 優先順位:
# - コマンド・ラインの --settings
# - 環境変数 DJANGO_SETTINGS_MODULE
# - ファイル secrets/environment の symlink 先から組み立てた DJANGO_SETTINGS_MODULE
#
SETTINGS_MODULE = detect_settings_module(base_dir)
os.environ.setdefault('DJANGO_SETTINGS_MODULE', SETTINGS_MODULE)

print("# DJANGO_SETTINGS_MODULE={}".format(os.environ.get('DJANGO_SETTINGS_MODULE')))
application = get_wsgi_application()

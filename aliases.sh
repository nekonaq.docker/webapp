export APP_DIR_WEBAPP="$( /bin/pwd )"

@webapp() {
  ( set -x
    cd "$APP_DIR_WEBAPP"
    docker-compose "$@"
  )
}

@webapp-devel() {
  @webapp -f docker-compose-devel.yml "$@"
}

@webapp.build() {
  @webapp -f docker-compose.yml -f docker-compose-build.yml build "$@"
}

@webapp.push() {
  ( . .env
    set -x
    docker push "$REGISTRY_PREFIX$COMPOSE_PROJECT_NAME:$IMAGE_VERSION"
  )
}

# usage:
#  @webapp up -d
#  @webapp up
#  @webapp run --rm webapp
#  @webapp run --rm webapp /bin/bash
#
#  @webapp.build
#
#  @webapp-devel run --rm webapp /bin/bash
#  @webapp-devel run --rm webapp bin/server
#  @webapp-devel run --rm webapp bin/runuwsgi
#  @webapp-devel run --rm webapp migrate
#  @webapp-devel run --rm webapp createsuperuser

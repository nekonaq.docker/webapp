from django.apps import AppConfig


class SiteAssetConfig(AppConfig):
    name = 'sitemain.asset'

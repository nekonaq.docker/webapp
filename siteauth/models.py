import uuid

from django.contrib.auth.models import AbstractUser, Group as auth_Group
from django.db import models
from django.utils.translation import gettext_lazy as _


class Group(auth_Group):
    orig_model = auth_Group

    class Meta:
        proxy = True
        verbose_name = auth_Group._meta.verbose_name
        verbose_name_plural = auth_Group._meta.verbose_name_plural


class User(AbstractUser):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    email = models.EmailField(
        verbose_name=_('email address'),
        unique=True,
    )

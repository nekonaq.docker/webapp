from django.contrib import admin
from django.contrib.auth.admin import (
    GroupAdmin as auth_GroupAdmin,
    UserAdmin as auth_UserAdmin,
)
from django.utils.translation import gettext_lazy as _

from .models import User, Group

admin.site.unregister(Group.orig_model)


@admin.register(Group)
class GroupAdmin(auth_GroupAdmin):
    pass


@admin.register(User)
class UserAdmin(auth_UserAdmin):
    list_display = (
        'email',
        'username',
        'last_name',
        'first_name',
        'last_login',
    )
    list_filter = (
        'is_active',
        'groups',
    )

    fieldsets = (
        (None, {
            'fields': (
                'email',
                'password',
                'username',
                'last_name',
                'first_name',
                'last_login',
                'date_joined',
            ),
        }),
        (_('Permissions'), {
            'fields': (
                'is_active',
                'groups',
            ),
        }),
    )
    readonly_fields = (
        'last_login',
        'date_joined',
    )

#!/usr/bin/env python3.6
import os
import sys
from djangoexpack.utils.settings import detect_settings_module


def main():
    base_dir = os.path.dirname(os.path.abspath(__file__))
    os.chdir(base_dir)

    # 優先順位:
    # - コマンド・ラインの --settings
    # - 環境変数 DJANGO_SETTINGS_MODULE
    # - ファイル secrets/environment の symlink 先から組み立てた DJANGO_SETTINGS_MODULE
    #
    SETTINGS_MODULE = detect_settings_module(base_dir)
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', SETTINGS_MODULE)

    print("# DJANGO_SETTINGS_MODULE={}".format(os.environ.get('DJANGO_SETTINGS_MODULE')))
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()

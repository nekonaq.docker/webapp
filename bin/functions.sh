ensure_root() {
  if [ "$( id -u )" != 0 ]; then
    echo "$0: You must be a root."
    return 1
  fi
}

set_django_settings() {
  local base_dir="$1" app_environ="$2"

  export BASE_DIR="$base_dir"
  local suffix="${prog#*.}"
  if [ "$suffix" = "$prog" ]; then
    suffix=
  fi

  local django_settings
  if [ -n "$app_environ" ]; then
    django_settings="siteconf.${suffix:-settings}.${app_environ}"
  elif [ -n "$DJANGO_SETTINGS_MODULE" ]; then
    django_settings="$DJANGO_SETTINGS_MODULE"
  else
    local envlink="$( readlink -e "$BASE_DIR/secrets/environments/default" || echo local )"
    django_settings="siteconf.${suffix:-settings}.${envlink##*/}"
  fi

  local envfile="$BASE_DIR/.env"
  if [ -f "$envfile" ]; then
    echo "# loading $envfile"
    . "$envfile"
  fi

  if [ -n "$django_settings" ]; then
    # .env の設定を上書き
    DJANGO_SETTINGS_MODULE="$django_settings"
  fi
  export DJANGO_SETTINGS_MODULE
}

FROM buildpack-deps:18.04

LABEL maintainer="Tatsuo Nakajyo <tnak@nekonaq.com>"

ARG DJANGO_SETTINGS_MODULE=siteconf.settings.local

ENV LANG C.UTF-8
ENV DJANGO_SETTINGS_MODULE $DJANGO_SETTINGS_MODULE

WORKDIR /app

COPY docker-entrypoint /
COPY requirements*.txt manage.py /app/
COPY bin /app/bin/
COPY config /app/config/
COPY siteconf /app/siteconf/
COPY siteadmin /app/siteadmin/
COPY siteauth /app/siteauth/
COPY dist /app/dist/

RUN set -x \
&& export DEBIAN_FRONTEND=noninteractive \
&& apt-get update \
&& apt-get upgrade -y \
&& apt-get install -y python3 python3-pip python3-apt libyaml-dev \
&& python3 -mpip install --upgrade pip setuptools ansible~=2.8.0 \
&& bin/install-prereq \
&& python3 -mpip install -r requirements.txt \
&& mkdir -p /app/data \
&& ./manage.py collectstatic --no-input \
&& ( set +x; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ) \
;

ENTRYPOINT ["/docker-entrypoint"]

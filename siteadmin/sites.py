from django.contrib import admin
from djangoexpack.admin import (
    AdminSiteTitleMixin,
    AdminSiteAppOrderingMixin,
)


class AdminSite(
        AdminSiteTitleMixin,
        AdminSiteAppOrderingMixin,
        admin.AdminSite
):
    site_url = None                        # 「View site」リンク非表示
    pass
